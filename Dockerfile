FROM python:3.11

RUN pip install poetry

WORKDIR /code
COPY ./pyproject.toml ./poetry.lock /code/
CMD poetry install

COPY . /code

CMD poetry shell

#COPY ./app.sh /code
#
#RUN chmod a+x *.sh