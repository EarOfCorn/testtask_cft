# Тестовое задание

Проект использует Python 3.11

## Инструкция по запуску

Для запуска REST-сервиса необходимо: 
1. Установить [`Poetry`](https://python-poetry.org/docs/#installation) 
2. Загрузить проект `git clone https://gitlab.com/EarOfCorn/testtask_cft.git`
3. Далее необходимо установить зависимости командой `poetry install`
4. Cоздать виртуальное окружение командой `poetry shell`

Потребуется зарегистрировать Базу данных, по шаблону: 
- Host name: localhost
- Port: 5432
- Maintance database: postgres
- Username: postgres
- Password: rootroot

Далее необходимо добавить таблицы в базу данных, для этого нужно использовать команду `alembic upgrade head`

Далее самой базе данных нужно дать запрос `insert into role values (1, 'admin', null), (2, 'user', null);`

Далее необходимо запустить сервер командой `uvicorn main:app --reload`

После этого можно зайти в браузер по адресу `http://localhost:8000/docs` для работы.


## Инструкция по использованию

В первую очередь нужно зарегистрировать пользователя в эндпоинте Registration:
- Развернув его, нажмите Try it out
- В появившемся поле задайте email, password, username, salary и raising_date, и подвердите кнопкой Execute
- После регистрации, можно войти использвуя почту и пароль

Далее нужно авторизоваться в первом эндпоинте Login/Logout:
- Развернув его, нажмите Try it out
- В поле username введите заданный ранее email, и пароль в password, и подвердите кнопкой Execute

В cookies появится код Token, который действителен в течении одного часа <br>
Во время его действия можно получить данные о зарплате и дате повышения, для этого перейдите к последнему эндпоинту default, GET запрос
- Развернув его, нажмите Try it out и сразу Execute

Ниже выдастся сообщение вроде "Hello, Matvey, your salary 60000, as well raising date 18.06.2023"

<br>
<br>

Сборка и запуск контейнера Docker не реализованы.


### Исполнитель: Калинченко Матвей - earofcorn8@gmail.com
