from fastapi import FastAPI, Request, status, Depends
from fastapi_users import fastapi_users, FastAPIUsers

from auth.cookie import auth_backend
from auth.database import User
from auth.manager import get_user_manager
from auth.schemas import UserRead, UserCreate

app = FastAPI(
    title='Salary_LevelUp'
)

fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["Login/Logout"],
)

app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["Registration"],
)

current_user = fastapi_users.current_user()

@app.get("/Getting_data")
def Getting_data(user: User = Depends(current_user)):
    return f"Hello, {user.username}, your salary {user.salary}, as well raising date {user.raising_date}"
